# Init
series = []
# Python average_min_max
print("Give me a series of numbers and I'll give you some info on them.")
while True:
    str = input()
    if not str:
        del(str)
        break
    str = int(str)
    series.append(str)
sum = sum(series)
average = sum / len(series)
min = min(series)
max = max(series)
print("Average:", average, "/ Minimum:", min, "/ Maximum:", max)

# bank note
bank = [10000, 5000, 2000, 1000, 100, 50, 10, 5, 1]
# input bill
bill = int(input("What's the bill ? "))
if(bill == 0):
    print("Oh, it was free !")
elif(bill < 0):
    print("Well, it's you who owes me money then.")
else:
    n_10000 = bill // 10000
    n_5000 = (bill % 10000) // 5000
    n_2000 = (bill % 5000) // 2000
    n_1000 = (bill % 2000) // 1000
    n_500 = (bill % 1000) // 500
    n_100 = (bill % 500) // 100
    n_50 = (bill % 100) // 50
    n_10 = (bill % 50) // 10
    n_5 = (bill % 10) // 5
    n_1 = (bill % 5)
    if(n_10000 != 0):
        print(n_10000, "10000 banknote")
    if(n_5000 != 0):
        print(n_5000, "5000 banknote")
    if(n_2000 != 0):
        print(n_2000, "2000 banknote")
    if(n_1000 != 0):
        print(n_1000, "1000 banknote")
    if(n_500 != 0):
        print(n_500, "500 banknote")
    if(n_100 != 0):
        print(n_100, "100 banknote")
    if(n_50 != 0):
        print(n_50, "50 banknote")
    if(n_10 != 0):
        print(n_10, "10 banknote")
    if(n_5 != 0):
        print(n_5, "5 banknote")
    if(n_1 != 0):
        print(n_1, "1 banknote")

# rock_paper_scissors
player_1 = input("Player 1? ")
player_2 = input("Player 2? ")
if(player_1 == "rock"):
    if(player_2 == "rock"):
        print("Draw.")
    elif(player_2 == "paper"):
        print("Player 2 wins.")
    elif(player_2 == "scissors"):
        print("Player 1 wins.")
    else:
        print("Error.")
elif(player_1 == "paper"):
    if(player_2 == "rock"):
        print("Player 1 wins.")
    elif(player_2 == "paper"):
        print("Draw.")
    elif(player_2 == "scissors"):
        print("Player 2 wins.")
    else:
        print("Error.")
elif(player_1 == "scissors"):
    if(player_2 == "rock"):
        print("Player 2 wins.")
    elif(player_2 == "scissors"):
        print("Draw.")
    elif(player_2 == "paper"):
        print("Player 1 wins.")
    else:
        print("Error.")
else:
    print("Error.")

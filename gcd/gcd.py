# gcd of two integers
a = int(input("a ? "))
b = int(input("b ? "))
if (a == 0 and b == 0):
    gcd = 0
elif (a <= b):
    if(b % a == 0):
        gcd = a
    else:
        for i in range(1, a // 2):
            if(a % i == 0):
                if(b % i == 0):
                    gcd = i
else:
    if (a % b == 0):
        gcd = b
    else:
        for i in range(1, b // 2):
            if(b % i == 0):
                if(a % i == 0):
                    gcd = i
print("gcd(a,b) =", gcd)

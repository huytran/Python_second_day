# pyramid
sizePyramid = int(input("What pyramid size do you want?\n"))

# modify size of Pyramid
if(sizePyramid % 2 == 0):
    sizePyramid -= 1
n = 0

# proccess
while True:
    print(" " * (sizePyramid // 2), "#" * (2*n + 1))
    n += 1
    sizePyramid -= 2
    if(sizePyramid < 0):
        break

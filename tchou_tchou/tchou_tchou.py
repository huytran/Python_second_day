# tchou_tchou
# Write a program that prints a train of various sizes.

top = " _________________________  "
mid = "|]||[]_[]_[]|||[]_[]_[]||[| "
bot = "\==o-o======o-o======o-o==/_"
top_1 = " ______________________>__"
mid_1 = "|]||[]_[]_[]|||[]_[]_[]||[| "
bot_1 = "\==o-o======o-o======o-o==/"

size = int(input("What is the size of your train? "))
if (size == 0):
    pass
elif(size < 0):
    print("Don't be so negative.")
else:
        print((size - 1) * top + top_1)
        print((size - 1) * mid + mid_1)
        print((size - 1) * bot + bot_1)
